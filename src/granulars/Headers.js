import React from 'react';
import { SafeAreaView, View, StyleSheet, ScrollView } from 'react-native';
import colors from '../utils/colors';

const Headers = (props) => {

    return (
        <SafeAreaView style={styles.mainView}>
            <ScrollView style={styles.scrollView}
                contentContainerStyle={styles.scrollviewContainer}>

                <View style={styles.inerView}>

                    {props.children}

                </View>
            </ScrollView>
        </SafeAreaView>
    )

}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.white
    },
    inerView: {
        marginLeft: 10,
        marginRight: 10
    },
    scrollView: {
        flex: 1
    },
    scrollviewContainer: {
        flexGrow: 1
    }
})
export default Headers;