import React from 'react';
import {View,Image,StyleSheet,Text} from 'react-native';
import colors from '../utils/colors';

const CircularImage = (props) =>{
    const {name,images} = props
    console.log(name+" "+images)
    return(

        <View style={styles.viewMain}>

        <View style={styles.viewCicular}>

        <Image style={styles.icons}
        source={images}/>
        </View>

        <Text style={styles.txtName}>{name}</Text>

        </View>

    )

}
const styles  = StyleSheet.create({
   viewMain:{
    flex:1,
    marginTop:10,
    flexDirection:'column',
    justifyContent:'center'
   },
    viewCicular:{
        width: 60,
        height: 60,
        borderRadius: 60 / 2,
        justifyContent:'center',    
        alignSelf:'center',
        backgroundColor: colors.primary 
    },
    icons:{
        width:20,
        height:20,
        alignSelf:'center'
    },
    txtName:{
        fontSize:15,
        color:colors.textGray,
        marginTop:5,
        alignSelf:'center'
    },
})
export default CircularImage;