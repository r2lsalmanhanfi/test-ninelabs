const colors = {
    primary:'#7E57C6',
    white:"#FFFFFF",
    black:'#000000',
    textGray:'#7A7985',
    text1C1C1C:"#1C1C1C",
    lightgray:"#585858",
    borderGray:'#D3D3D3',
  }
  
  export default colors