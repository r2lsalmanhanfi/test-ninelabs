import React from 'react';
import {
  View,
  StyleSheet,
  Image, Text,
} from 'react-native';
import CircularImages from './src/granulars/CircularImages';
import Headers from './src/granulars/Headers.js';
import colors from './src/utils/colors';
import Conts from './src/utils/Conts';
import * as ImageAssets from './src/utils/ImageAssets';

const App = () => {

  return (

    <Headers>
      <View style={styles.viewHeader}>

        <View style={styles.profileBorder}>
          <Image style={styles.profileImage}
            source={ImageAssets.ic_profileLogo}></Image>
        </View>
        <Text style={styles.textName}>{Conts.Name}</Text>

        <Text style={styles.textTime}>{Conts.time}</Text>

        <View style={styles.viewRow}>

          <CircularImages
            name={Conts.videocall}
            images={ImageAssets.ic_videocall} />

          <CircularImages
            name={Conts.hold}
            images={ImageAssets.ic_hold} />

          <CircularImages
            name={Conts.message}
            images={ImageAssets.ic_message} />

        </View>

        <View style={styles.viewRow}>

          <CircularImages
            name={Conts.speaker}
            images={ImageAssets.ic_speaker} />

          <CircularImages
            name={Conts.recording}
            images={ImageAssets.ic_recording} />

          <CircularImages
            name={Conts.add_call}
            images={ImageAssets.ic_add_call} />
        </View>

        <Image style={styles.imgCall}
          source={ImageAssets.ic_telephone}>
        </Image>

      </View>
    </Headers>
  )
}

const styles = StyleSheet.create({

  scrollView: {
    flex: 1
  },
  viewHeader: {
    marginTop: 10
  },
  profileImage: {
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
    overflow: "hidden",
    alignSelf: 'center'
  },
  profileBorder: {
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
    overflow: "hidden",
    borderWidth: 3,
    marginTop: 40,
    alignSelf: 'center',
    borderColor: colors.primary
  },
  textName: {
    fontSize: 16,
    color: colors.black,
    alignSelf: 'center',
    marginTop: 5,
    fontWeight: '700'
  },
  textTime: {
    fontSize: 14,
    alignSelf: 'center',
    marginTop: 5,
    color: colors.textGray
  },
  viewRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
    justifyContent: 'center'
  },
  imgCall: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 40
  }

})
export default App;
